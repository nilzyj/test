package com.example.zyj.rssdemo;

/**
 * Created by dim on 2016/8/17.
 */
public class RSSItem {
    public static final String TITLE = "titel";
    public static final String PUBDATE = "pubdate";
    private String title = null;
    private String description = null;
    private String link = null;
    private String category = null;
    private String pubdate = null;
}
