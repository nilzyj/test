package com.zyj.phonemessage;

import android.app.Activity;
import android.os.Bundle;

/**
 * 绑定界面Activity
 */
public class Binding extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding);
    }
}
